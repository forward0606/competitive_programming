#include<iostream>
#include<algorithm>
using namespace std;


pair<int, int> v[2000006];

int main(){
	long long n;
	cin>>n;
	for(int i=0, l, r;i<n;i++){
		cin>>l>>r;
		v[i*2] = {l, 1};
		v[i*2+1] = {r, -1};
	}
	sort(v, v+2*n);
	long long sum = 0, cnt = 0;
	int idx = 0;
	for(int i=0;i<v[2*n-1].first && idx < 2*n;i++){
		while(v[idx].first <= i && idx < 2*n){
			cnt += v[idx].second;
			idx++;
		}
		sum += cnt * cnt;
	}
	cout<<sum<<'\n';
	return 0;
}
