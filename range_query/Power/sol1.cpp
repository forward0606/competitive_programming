#include<iostream>
using namespace std;


long long a[1000006];

int main(){
	int n, on, off;
	cin>>n;
	int mx = 0;
	for(int i=0;i<n;i++){
		cin>>on>>off;
		a[on] += 1;
		a[off] -= 1;
		mx = max(mx, off);
	}
	long long sum = 0;
	for(int i=0;i<mx;i++){
		sum += a[i] * a[i];
		a[i+1] += a[i];
	}
	cout<<sum<<'\n';
	return 0;
}
