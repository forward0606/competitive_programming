#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

vector<pair<int, int>> v;

int main(){
	long long n, w, s, t, p;
	cin>>n>>w;
	for(int i=0;i<n;i++){
		cin>>s>>t>>p;
		v.emplace_back(s, p);
		v.emplace_back(t, -p);
	}
	sort(v.begin(), v.end());
	long long cnt = 0;
	bool flag = true;
	for(int i=0;i<v.size();i++){
		cnt += v[i].second;
		while(i+1 < v.size() && v[i].first == v[i+1].first){
			i++;
			cnt += v[i].second;
		}
		if(cnt > w){
			flag = false;
		}
	}
	cout<<(flag?"Yes":"No")<<endl;
	return 0;
}
