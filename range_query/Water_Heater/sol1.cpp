#include<iostream>
using namespace std;

long long a[200005];

int main(){
	int N;
	long long W, s, t, p, mx = 0;
	cin>>N>>W;
	for(int i=0;i<200005;i++){
		a[i] = 0;
	}
	for(int i=0;i<N;i++){
		cin>>s>>t>>p;
		a[s] += p;
		a[t] -= p; 
		mx = max(t, mx);
	}
	bool flag = true;
	for(int i=0;i<=mx;i++){
		a[i] += a[i-1];
		if(a[i] > W){
			flag = false;
		}
	}
	cout<<(flag?"Yes":"No")<<'\n';
	return 0;
}
