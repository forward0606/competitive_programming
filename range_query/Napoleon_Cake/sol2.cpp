#include<iostream>
using namespace std;

int a[200005], ans[200005];

int main(){
	int T, n;
	cin>>T;
	while(T--){
		cin>>n;
		for(int i=0;i<n;i++){
			ans[i] = 0;
		}
		for(int i=0;i<n;i++){
			cin>>a[i];
			ans[max(i-a[i]+1, 0)] += 1;
			ans[i+1] -= 1;
		}
		for(int i=0;i<n;i++){
			cout<<(ans[i] != 0)<<' ';
			if(i != n-1)ans[i+1] += ans[i];
		}
		cout<<'\n';
	}
	return 0;
}
