#include<iostream>
using namespace std;

int a[200005], ans[200005];

int main(){
	int T, n;
	cin>>T;
	while(T--){
		cin>>n;
		for(int i=0;i<n;i++){
			cin>>a[i];
			ans[i] = 0;
		}
		int l = n;
		for(int i=n-1;i>=0;i--){
			l = min(l, i - a[i]+1);
			if(i >= l){
				ans[i] = 1;
			}
		}
		for(int i=0;i<n;i++){
			cout<<ans[i]<<" ";
		}
		cout<<'\n';
	}
	return 0;
}
