# problem set
## bf(brute force)
### 二分枚舉:
- Meet in the Middle
	- https://cses.fi/problemset/task/1628
	- https://cses.fi/paste/292818340b4807633cd5ea/
- Maximum Subsequence
    - https://codeforces.com/problemset/problem/888/E
    - https://codeforces.com/contest/888/submission/158410285
- Xor-Paths
    - https://codeforces.com/problemset/problem/1006/F
    - https://codeforces.com/contest/1006/submission/158413828
    - https://codeforces.com/contest/1006/submission/158414266

## graph:
### 0-1 BFS
- https://codeforces.com/blog/entry/22276
- Jailbreak
    - https://codeforces.com/gym/100625/problem/J

## range query
### 區間求和
- GeT AC
	- https://atcoder.jp/contests/abc122/tasks/abc122_c
	- https://atcoder.jp/contests/abc122/submissions/31976178
### 離線技巧
- Range Count Query

### 差分序列
- Napoleon Cake
	- https://codeforces.com/contest/1501/problem/B
	- naive solution : https://codeforces.com/contest/1501/submission/158551225
	- 差分序列 : https://codeforces.com/contest/1501/submission/158552433
- Power
	- https://codeforces.com/group/gRkn7bDfsN/contest/212178/problem/B
	- 差分序列解 : https://codeforces.com/group/gRkn7bDfsN/contest/212178/submission/158553411
	- sorting 解 : https://codeforces.com/group/gRkn7bDfsN/contest/212178/submission/158555440
	- 神奇的 bug : https://codeforces.com/group/gRkn7bDfsN/contest/212178/submission/158555190
- Water Heater
	- https://atcoder.jp/contests/abc183/tasks/abc183_d
	- 差分序列解 : https://atcoder.jp/contests/abc183/submissions/31975785
	- sorting 解 : https://atcoder.jp/contests/abc183/submissions/31975902
