#include<iostream>
#include<set>
using namespace std;


int n, m;
int a[40];
set<int> st;
long long ans = 0;
 
void dfs1(int idx, long long sum){
	if(idx == n/2){
		sum %= m;
		st.insert(sum);
		ans = max(ans, sum);
		return;
	}
	dfs1(idx+1, sum);
	dfs1(idx+1, sum + a[idx]);
}
 
void dfs2(int idx, long long sum){
	if(idx == n){
		sum %= m;
		auto it = st.lower_bound(m - sum);
		it --;
		ans = max(ans, (*it + sum) % m);
		return;
	}
	dfs2(idx+1, sum);
	dfs2(idx+1, sum + a[idx]);
}


int main(){
	cin>>n>>m;
	for(int i=0;i<n;i++){
		cin>>a[i];
	}
	dfs1(0, 0);
	dfs2(n/2, 0);
	cout<<ans<<'\n';
	return 0;
}
