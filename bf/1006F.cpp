#include<iostream>
#include<map>
using namespace std;

int n, m;
long long k;
long long a[25][25];
long long ans = 0;
map<long long, long long> mp[25][25];

void dfs1(int x, int y, long long Xor){
	if(x >= n)	return;
	if(y >= m)	return;
	if(x+y+1 == (n+m)/2){
		mp[x][y][Xor] += 1;
		return;
	}
	Xor ^= a[x][y];
	dfs1(x, y+1, Xor);
	dfs1(x+1, y, Xor);
}
 
void dfs2(int x, int y, long long Xor){
	if(x < 0 || y < 0)	return;
	if(x+y+1 == (m+n)/2){
		Xor ^= a[x][y] ^ k;
		ans += mp[x][y][Xor];
		return;
	}
	Xor ^= a[x][y];
	dfs2(x, y-1, Xor);
	dfs2(x-1, y, Xor);
}


int main(){
	cin>>n>>m>>k;
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			cin>>a[i][j];
		}
	}
	/*if(n == 1 && m == 1){
		cout<<(a[0][0] == k)<<'\n';
		return 0;
	}*/
	dfs1(0, 0, 0);
	dfs2(n-1, m-1, 0);
	cout<<ans<<'\n';
	return 0;
}
